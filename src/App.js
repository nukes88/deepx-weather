import React from "react";

import Main from "./Screens/Main";

import "./App.css";
import WeatherContainer from "./Containers/WeatherContainer";

function App() {
  return (
    <WeatherContainer.Provider>
      <Main />
    </WeatherContainer.Provider>
  );
}

export default App;
