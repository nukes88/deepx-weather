import { useState } from "react";
import { createContainer } from "unstated-next";
import { WEATHER_API_KEY, WEATHER_API_URL } from "../Config/common";

function useWeather() {
  let [cities, setCities] = useState([]);

  async function getCityWeather(city) {
    try {
      let fetchURL = `${WEATHER_API_URL}?q=${city}&appid=${WEATHER_API_KEY}&units=metric`;

      let response = await fetch(fetchURL);
      if (response.ok) {
        let data = await response.json();
        return data;
      }
      throw new Error(`Fetch city weather error: ${response.statusText}`);
    } catch (e) {
      console.error(e);
      return false;
    }
  }

  function addCity(city) {
    setCities([...cities, city]);
  }

  function removeCity(cityId) {
    setCities(cities.filter((c) => c.id !== cityId));
  }

  return {
    cities,
    addCity,
    removeCity,
    getCityWeather,
  };
}

const WeatherContainer = createContainer(useWeather);
export default WeatherContainer;
