import React from "react";
import { Card } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";

export default function NewTile({ clickHandler }) {
  return (
    <Card onClick={clickHandler} className="city-tile-new">
      <Card.Text>
        <strong>Add new city</strong>
      </Card.Text>
      <FontAwesomeIcon icon={faPlus} size="4x" />
    </Card>
  );
}
