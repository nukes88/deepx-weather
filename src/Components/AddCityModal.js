import React, { useEffect, useRef, useState } from "react";
import { Button, Col, Form, Modal, Row } from "react-bootstrap";

export default function AddCityModal({ show, handleClose, handleAdd }) {
  const defaultRefresh = 5;
  let [city, setCity] = useState("");
  let [refresh, setRefresh] = useState(defaultRefresh);
  let [error, setError] = useState("");
  const cityInput = useRef(null);

  function reset() {
    setCity("");
    setError("");
    setRefresh(defaultRefresh);
  }

  useEffect(() => {
    if (show && cityInput.current) {
      reset();
      cityInput.current.focus();
    }
  }, [show]);

  function cityChange(evt) {
    // not gonna check for funny chars
    setCity(evt.target.value);
  }

  function finish() {
    if (city.trim() !== "") {
      handleAdd({
        city,
        refresh,
      });
    } else {
      setError("Don't put nothing");
    }
  }

  function handleKeyPress(evt) {
    if (evt.key === "Enter") {
      finish();
    }
  }

  function handleIntervalChange(evt) {
    setRefresh(evt.target.value);
  }

  return (
    <Modal show={show} onHide={handleClose} centered>
      <Modal.Header closeButton>
        <Modal.Title>Add City</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form.Group as={Row}>
          <Form.Label as={Col} md="2" className="text-right">
            City:
          </Form.Label>
          <Col md="10">
            <Form.Control
              type="text"
              value={city}
              onChange={cityChange}
              onKeyPress={handleKeyPress}
              ref={cityInput}
              isInvalid={error !== ""}
            />
            <Form.Control.Feedback type="invalid">
              {error}
            </Form.Control.Feedback>
            <Form.Text>Will not check for valid cities here</Form.Text>
          </Col>
        </Form.Group>
        <Form.Group as={Row}>
          <Form.Label as={Col} md="2" className="text-right">
            Interval:
          </Form.Label>
          <Col md="10">
            <Form.Control
              as="select"
              value={refresh}
              onChange={handleIntervalChange}
            >
              <option value="5">5 seconds</option>
              <option value="10">10 seconds</option>
              <option value="30">30 seconds</option>
              <hr />
              <option value="60">1 minute</option>
              <option value="120">2 minutes</option>
            </Form.Control>
          </Col>
        </Form.Group>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" onClick={finish}>
          <strong>Add</strong>
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
