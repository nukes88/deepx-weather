import React, { useEffect, useState } from "react";
import { Card, Alert, Spinner } from "react-bootstrap";
import { faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { WEATHER_ICON_URL } from "../Config/common";

import WeatherContainer from "../Containers/WeatherContainer";

function Temperature({ temp }) {
  return (
    <div className="d-flex align-items-center city-tile-temperature">
      {Math.ceil(temp)} <span className="degrees">&#176;C</span>
    </div>
  );
}

export default function CityTile({ id, city, refresh }) {
  const weather = WeatherContainer.useContainer();

  let [showDeleteOverlay, setShowDeleteOverlay] = useState(false);
  let [loadingCityWeather, setLoadingCityWeather] = useState(false);
  let [loadingImage, setLoadingImage] = useState(true);
  let [cityWeather, setCityWeather] = useState();
  let [error, setError] = useState("");
  let [refreshInterval, setRefreshInterval] = useState();

  useEffect(() => {
    let isUnmounted = false;

    // call once
    getWeather();

    // set the refresh interval
    setRefreshInterval(
      setInterval(() => {
        if (!isUnmounted) {
          getWeather();
        }
      }, parseInt(refresh, 10) * 1000)
    );

    return () => {
      isUnmounted = true;
      clearInterval(refreshInterval);
    };
  }, []);

  async function getWeather() {
    setLoadingCityWeather(true);
    let _cityWeather = await weather.getCityWeather(city);
    console.log(_cityWeather.weather);
    if (_cityWeather === false) {
      setError(`Error fetching weather for ${city}!`);
    } else {
      let { weather, main } = _cityWeather;
      let summarisedWeather = {
        weather: weather[0],
        main,
      };
      console.log(summarisedWeather);
      setCityWeather(summarisedWeather);
    }
    setLoadingCityWeather(false);
  }

  function handleTileClick() {
    weather.removeCity(id);
  }

  function handleMouseOver() {
    setShowDeleteOverlay(true);
  }

  function handleMouseLeave() {
    setShowDeleteOverlay(false);
  }

  return (
    <Card
      className="city-tile"
      onClick={handleTileClick}
      onMouseOver={handleMouseOver}
      onMouseLeave={handleMouseLeave}
    >
      {showDeleteOverlay ? (
        <Card.Body className="city-tile-delete-overlay">
          <Card.Title>Delete {city.toUpperCase()}?</Card.Title>
          <Card.Text className="d-flex justify-content-center">
            <FontAwesomeIcon
              icon={faTrashAlt}
              size="4x"
              className="city-tile-delete-icon"
            />
          </Card.Text>
        </Card.Body>
      ) : loadingCityWeather ? (
        <Card.Body className="d-flex align-items-center justify-content-center">
          <Spinner animation="border" />
        </Card.Body>
      ) : (
        <>
          <Card.Body>
            <Card.Title>{city.toUpperCase()}</Card.Title>
            {error ? (
              <Alert variant="danger">{error}</Alert>
            ) : !cityWeather || !cityWeather.weather ? (
              <div className="d-flex align-items-center justify-content-center">
                <Spinner animation="border" />
              </div>
            ) : (
              <>
                <Card.Img
                  onLoadStart={() => setLoadingImage(true)}
                  onLoad={() => setLoadingImage(false)}
                  variant="top"
                  src={`${WEATHER_ICON_URL}${cityWeather.weather.icon}@4x.png`}
                />
                {loadingImage ? <Spinner animation="border" /> : null}
                <Card.Text>
                  <div className="d-flex justify-content-center">
                    <Temperature temp={cityWeather.main.temp} />
                  </div>
                </Card.Text>
              </>
            )}
          </Card.Body>
        </>
      )}
    </Card>
  );
}
