import React, { useState } from "react";
import { v4 as uuidv4 } from "uuid";

import { CSSTransition, TransitionGroup } from "react-transition-group";
import CityTile from "../Components/CityTile";
import NewTile from "../Components/NewTile";
import AddCityModal from "../Components/AddCityModal";
import WeatherContainer from "../Containers/WeatherContainer";

export default function Main() {
  const weather = WeatherContainer.useContainer();

  let [showAddCityModal, setShowAddCityModal] = useState(false);

  function addNewCity({ city, refresh }) {
    weather.addCity({
      id: uuidv4(),
      city,
      refresh,
    });
    setShowAddCityModal(false);
  }

  function closeModal() {
    setShowAddCityModal(false);
  }

  return (
    <TransitionGroup className="main">
      <AddCityModal
        show={showAddCityModal}
        handleClose={closeModal}
        handleAdd={addNewCity}
      />
      {weather.cities && weather.cities.length > 0
        ? weather.cities.map((t) => (
            <CSSTransition key={t.id} timeout={200} classNames="weather-tiles">
              <CityTile {...t} />
            </CSSTransition>
          ))
        : null}
      <NewTile clickHandler={() => setShowAddCityModal(true)} />
    </TransitionGroup>
  );
}
