# DeepX Weather Assignment

- A horizontal list of city tiles, displaying their weather information

# How to run

- clone the repo
- run `yarn`
- run `yarn start`

# Setup

- used create-react-app
- used boostrap and the [react-bootstrap](https://react-bootstrap.github.io/) library for prebuilt bootstrap components
- used [react-transition-group](http://reactcommunity.org/react-transition-group/) for animations
- used [unstated-next](https://github.com/jamiebuilds/unstated-next) instead of redux, as that has a ton of unnecessary boilerplate for a small project like this
- split folder structure to Screens, Components, Containers and Config

# Notes

- have demonstrated most of the commonly used Hook APIs (useState, useEffect, useRef)
